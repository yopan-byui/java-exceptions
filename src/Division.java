import java.util.Scanner;

public class Division {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean applicationFacedAProblem = true;

        do {
            Float num1 = getInputNumber(scanner, "first");

            Float num2 = getInputNumber(scanner, "second");

            try {
                float result = doDivision(num1, num2);
                System.out.println("The result of the division is: " + result);

                applicationFacedAProblem = false;
            } catch (ArithmeticException e) {
                System.out.println("An arithmetic problem has been encountered. You might have tried to divide by 0.");
            } finally {
                if(applicationFacedAProblem) {
                    System.out.println("Please try again!");
                } else {
                    System.out.println("Success! See you later.");
                }
            }

        } while (applicationFacedAProblem);
    }

    public static float doDivision(Float num1, Float num2) throws ArithmeticException{
        return num1 / num2;
    }

    public static float getInputNumber(Scanner scanner, String whichNumber) {
        boolean wrongFirstInputFormat = true;
        String input;
        Float number = null;
        System.out.print("Please provide the " + whichNumber + " number: ");
        do {
            input = scanner.nextLine();
            if (denominatorIsInValid(whichNumber, input)) {
                System.out.println("The denominator can't be 0 (zero). Please provide a valid denominator.");
                System.out.print("Provide the " + whichNumber + " number: ");
            } else if(isInputANumber(input)) {
                wrongFirstInputFormat = false;
                number = Float.parseFloat(input);
            } else {
                System.out.println("That was not a valid input. Please try again.");
                System.out.print("Provide the " + whichNumber + " number: ");
            }

        } while (wrongFirstInputFormat);

        return number;
    }

    public static boolean denominatorIsInValid(String whichNumber, String input) {
        if (whichNumber.equals("second") && input.equals("0")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isInputANumber (String input) {
        try {
            Float.parseFloat(input);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
}
