import java.util.Scanner;

public class DivisionSimpler {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean applicationFacedAProblem = true;

        do {
            System.out.print("Please provide the first number: ");
            Integer num1 = Integer.parseInt(scanner.nextLine());

            System.out.print("Please provide the second number: ");
            Integer num2 = Integer.parseInt(scanner.nextLine());

            try {
                float result = doDivision(num1, num2);

                applicationFacedAProblem = false;
                System.out.println("The result of the division is: " + result);
            } catch (ArithmeticException e) {
                System.out.println("An arithmetic problem has been encountered. You might have tried to divide by 0.");
            } finally {
                if(applicationFacedAProblem) {
                    System.out.println("Please try again!");
                } else {
                    System.out.println("Success! See you later.");
                }
            }
        } while (applicationFacedAProblem);
    }

    public static float doDivision(Integer num1, Integer num2) throws ArithmeticException{
        return num1 / num2;
    }
}